package ro.sda.eventsFinalProject.service;

import org.springframework.stereotype.Service;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.repository.EventRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventService {

    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Event saveEvent(Event event) {
        if(event == null){
            throw new IllegalArgumentException("An event must have body!");

        }
        if (event.getName() == null) {
            throw new IllegalArgumentException("An event must have a name!");
        }
        if (event.getStartDate() == null || event.getEndDate() == null || event.getEndDate().isAfter(event.getEndDate())) {
            throw new IllegalArgumentException("Start date is after end date. Please be careful!");
        }
        Event savedEvent = eventRepository.save(event);
        return savedEvent;

    }

    public Event readEvent(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("Event id must not be null");
        }
        Event event = eventRepository.findById(id).orElse(null);
        if (event == null) {
            throw new IllegalArgumentException("There is no event with ID:" + id);
        }
        return event;

        /* sau:
        try {
    Event event = eventRepository.findById(id).get();
    return event;
        }    catch (NoSuchElementException e) {
    throw new IllegalArgumentException("There is no event with id " + id);
    }*/
    }

    public List<Event> readAllEvents() {
        return eventRepository.findAll();
    }

    public Event updateEvent(Event updatedEvent) {
        // ca sa verificam ca exista un event ci id-ul dat in baza de date
        if(updatedEvent == null){
            throw new IllegalArgumentException("An event must have body..");

        }
        Event eventToUpdate = readEvent(updatedEvent.getId());
        // salvam (cu toate verificarile de mai sus) evenimentul in baza de date
        return eventRepository.save(updatedEvent);
    }
    public void deleteEvent(Integer eventId){
        Event eventToDelete = readEvent(eventId);
        eventRepository.delete(eventToDelete);
    }
}
